fromAll().when({
  $init: function () {
    return {
      count: 0,
      AQ: 0,
      correction: 0,
    };
  },
  $any: function (s, e) {
    s.count += 1;
  },
  AQ: function (s, e) {
    s.AQ += 1;
  },
  correction: function (s, e) {
    s.correction += 1;
  },
}).transformBy(function(state){
	state.ratio = state.correction/state.count;
	return state;
});
