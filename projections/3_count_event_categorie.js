fromCategory('tree')
.when({
    $init: function(){
        return {
            count: 0,
            mostRecent:null
        }
    },
    "AQ": function (s, e) {
    s.count += 1;
    s.mostRecent = e.data.tree
  },
})