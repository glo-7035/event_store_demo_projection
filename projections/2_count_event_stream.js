fromStream("state-1")
.when({
  $init: function () {
    return {
      count: 0,
      states:{}
    };
  },
  $any: function (s, e) {
    s.count += 1;
    if (s.states.hasOwnProperty(e.data.state)){
      s.states[e.data.state]+=1;
    }else{
      s.states[e.data.state]=0;
    }  
  },
});
