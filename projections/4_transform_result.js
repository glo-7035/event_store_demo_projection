fromStreams(["tree-1","tree-2"])
.when({
  $init: function () {
    return {
      number: 0,
    };
  },
  $any: function (s, e) {
    s.number += 1;
  },
}).transformBy(function(state){
	state.priceByTree = 100;
	state.earning = state.number*100;
	state.earning = String(state.earning)+"$";
	return state;
});
