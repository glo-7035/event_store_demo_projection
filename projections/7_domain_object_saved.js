fromStream('$bc-4')
.when({
    $init: function(){
        return {
            tree:null,
            long:null,
            lat:null,
            state:null,
        }
    },
    "$any": function (s, e) {
    if(e.data.hasOwnProperty("tree")){
        s.tree=e.data.tree
    }
    if(e.data.hasOwnProperty("long")){
        s.long=e.data.long
    }
    if(e.data.hasOwnProperty("lat")){
        s.lat=e.data.lat
    }
    if(e.data.hasOwnProperty("state")){
        s.state=e.data.state
    }
  },
})