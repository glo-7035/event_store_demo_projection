fromStream('tree-4')
.when({
    $init: function(){
        return {
            aq:{},
            correction:{}
        }
    },
    "AQ": function (s, e) {
        s.aq[e.metadata.eventId] =e.data
  },
  "correction":function(s,e){
      s.correction[e.metadata.corretionOf] =e
  }
}).transformBy(function(s){
    s.valid=s.aq;
    for (const eventId in s.correction) {
        delete s.valid[eventId]
        id = s.correction[eventId].metadata.eventId;
        s.valid[id]= s.correction[eventId].data;
    }
	return s;
});