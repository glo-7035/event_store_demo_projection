use json;
use reqwest;
use text_io::read;

fn main() {
    println!("----------LECTURE DU STREAM `tree-4`----------");
    get_stream(STREAM);
    let _i: String = read!();
    println!("----------LECTURE DE EVENEMENT 1 DE `tree-4`----------");
    get_event(STREAM, 1);
    let _i: String = read!();
    println!("----------LECTURE DE EVENEMENT 32 DE `tree-4`----------");
    get_event(STREAM, 32);
    let _i: String = read!();
    println!("----------LECTURE DE SUBSCRIPTION TREE----------");
    get_subscription(STREAM, SUBSCRIPTION);
    let _i: String = read!();
    println!("----------LECTURE DE PROJECTION domain_object----------");
    get_projection(PROJECTION_DOMAIN_OBJECT);
}

fn get_projection(projection: &str) {
    let client = reqwest::blocking::Client::new();

    let addr = format!(
        "{server}/projection/{projection}/state",
        server = SERVER_ADDR,
        projection = projection,
    );

    let res = client
        .get(addr.as_str())
        .basic_auth(USER, Some(PASSWORD))
        .send();
    println!("query: {}\n", addr);
    let json = json::parse(res.unwrap().text().unwrap().as_str()).unwrap();
    println!("resp:\n{}", json.pretty(1));
}

fn get_subscription(stream: &str, subscription_name: &str) {
    let client = reqwest::blocking::Client::new();

    let addr = format!(
        "{server}/subscriptions/{stream}/{subscription}/{count}?embed={embed}",
        server = SERVER_ADDR,
        stream = stream,
        subscription = subscription_name,
        embed = EMBED,
        count = COUNT,
    );

    let res = client
        .get(addr.as_str())
        .basic_auth(USER, Some(PASSWORD))
        .header("Accept", CONTENT_TYPE_PROJECTION)
        .send();
    println!("query: {}\n", addr);
    let json = res.unwrap().text().unwrap();
    println!("resp:\n{}", json);
}

fn get_stream(stream: &str) {
    let client = reqwest::blocking::Client::new();

    let addr = format!(
        "{server}/streams/{stream}",
        server = SERVER_ADDR,
        stream = stream,
    );

    let res = client
        .get(addr.as_str())
        .basic_auth(USER, Some(PASSWORD))
        .header("Accept", CONTENT_TYPE_GET)
        .send();
    println!("query: {}\n", addr);
    let json = res.unwrap().text().unwrap();
    println!("resp:\n{}", json);
}

fn get_event(stream: &str, i: u32) {
    let client = reqwest::blocking::Client::new();

    let addr = format!(
        "{server}/streams/{stream}/{i}",
        server = SERVER_ADDR,
        stream = stream,
        i = i
    );

    let res = client
        .get(addr.as_str())
        .basic_auth(USER, Some(PASSWORD))
        .header("Accept", CONTENT_TYPE_GET)
        .send();
    println!("query: {}\n", addr);
    let json = res.unwrap().text().unwrap();
    println!("resp:\n{}", json);
}

const SERVER_ADDR: &str = "http://localhost:2113";
const USER: &str = "admin";
const PASSWORD: &str = "changeit";
const PROJECTION_DOMAIN_OBJECT: &str = "domain_object";
const EMBED: &str = "PrettyBody";
const COUNT: &str = "3";
const CONTENT_TYPE_PROJECTION: &str = "application/vnd.eventstore.competingatom+json";
const CONTENT_TYPE_GET: &str = "application/vnd.eventstore.atom+json";

const STREAM: &str = "tree-4";
const SUBSCRIPTION: &str = "tree";
